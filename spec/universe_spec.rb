require 'universe'
RSpec.describe Universe::World do
    it "broccoli is gross" do
      expect(Universe::World.portray("Broccoli")).to eql("Broccoli Gross!")
    end
  
    it "anything else is delicious" do
      expect(Universe::World.portray("Not Broccoli")).to eql("Delicious!")
    end

    it "pluralizes a word" do
        expect(Universe::World.pluralize("Tomato")).to eql("Tomatoes")
    end
    it "pluralizes a word" do
      expect(Universe::World.pluralize(nil)).to eql("Enter the correct input")
  end
end