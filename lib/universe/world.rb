require 'active_support/inflector'
module Universe
    class World
      def self.portray(world)
        if world.downcase == "broccoli"
          "#{world} Gross!"
        else
          "Delicious!"
        end
      end
      def self.pluralize(word)
        if word.nil?
          "Enter the correct input"
        else
          word.pluralize
        end

      end
    end
end