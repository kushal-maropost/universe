require 'universe'
require 'thor'
require 'universe/generators/recipe'
module Universe
  class CLI < Thor
    desc "portray ITEM", "Determines if a piece of food is gross or delicious"
    def portray(name)
      puts Universe::World.portray(name)
    end
    desc "pluralize", "Pluralizes a word"
    method_option :word, aliases: "-w"
    def pluralize
        puts Universe::World.pluralize(options[:word])
    end
    desc "recipe", "Generates a recipe scaffold"
    def recipe(group, name)
      Universe::Generators::Recipe.start([group, name])
    end
    desc "convert num", "It will convert string into integer"
    def convert(name)
      case name
      when "one"
        puts "1"
      when "two"
        puts "2"
      when "three"
        puts "3" 
      else
        puts "#{name} invalid"
      end 
    end
  end
end