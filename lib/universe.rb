# frozen_string_literal: true

require_relative "universe/version"
require 'universe/world'

module Universe
  class Error < StandardError; end
  # Your code goes here...
end
